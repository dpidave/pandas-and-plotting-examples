# Learning by example python pandas and python plotting  
A set of notebooks that I can use to add code examples of doing interesting
things in python and pandas.  

## Requirements  
Best way is to install [miniconda](https://conda.io/miniconda.html) as this
includes pandas and jupyter-notebook.  

Also install python seaborn to make the plots look a lot nicer.
``` pip install seaborn```  

## pandas-by-example  
This is a set of examples of dataframe manipulation using python pandas.  

## python-graphing-by-example  
Some examples of plots using python.  

## Use conda to clone env   
```
conda env create --file env/spec-file.txt
```

## Run in a notebook  
```
jupyter-notebook
```
